{% docs netsuite_stitch_expenses %}
To protect the information of employees who are Contractors, we mask any data that has `Contract` in the memo.
{% enddocs %}

{% docs netsuite_stitch_transaction_lines %}
To protect the information of employees who are Contractors, we mask any data that has `Contract` in the memo.
{% enddocs %}

{% docs netsuite_stitch_transaction_items %}
The itemslist field contains transaction details for invoices. This model provides the details for invoice transactions.
{% enddocs %}
